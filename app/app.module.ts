import { NgModule, enableProdMode } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { FormsModule }   from '@angular/forms';
import { RouterModule }   from '@angular/router';
import { routing } from './app.routes';
import { AppComponent }  from './app.component';
import { HomeComponent } from './pages/home/home.component';
import { PageNotFoundComponent } from './pages/error/error.component';
import { SearchComponent } from './pages/search/search.component';
import { FilterPipe } from './pipes/filter.pipe';


enableProdMode();
@NgModule({
  imports: [ 
  	BrowserModule, 
  	HttpModule, 
  	FormsModule,
    routing,
  ],
  declarations: [ AppComponent, HomeComponent, SearchComponent, PageNotFoundComponent, FilterPipe ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
