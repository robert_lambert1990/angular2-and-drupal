import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './pages/home/home.component';
import { SearchComponent } from './pages/search/search.component';
import { PageNotFoundComponent } from './pages/error/error.component';


const routes: Routes = [

  {
    path: '',
    component: HomeComponent,
  },

  {
    path: 'search',
    component: SearchComponent,
  },

  {
    path: '**',
    component: PageNotFoundComponent
  },

];

export const routing = RouterModule.forRoot(routes);