import { Injectable }     from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import {Observable} from 'rxjs/Rx';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class DemoService {

    constructor (private http: Http) {}

    private topLevelCategoriesUrl = 'http://www.drupalconfig-prod.mk2/drupal/api/top-level-categories/';
    private tutorialsUrl = 'http://www.drupalconfig-prod.mk2/drupal/api/all-tutorials/';
    private postUrl = 'http://www.drupalconfig-prod.mk2/drupal/entity/node/';
    private tutorialSelectedUrl = 'http://www.drupalconfig-prod.mk2/drupal/api/content-related-to-taxonomy/'

    getTopLevelCategories() {
        let temp = this.http.get(this.topLevelCategoriesUrl).map((res:Response) => res.json());
        return temp;
    }

    getTutorialsFromSelected(tid) {
        let temp = this.http.get(this.tutorialSelectedUrl + tid).map((res:Response) => res.json());
        return temp;
    }

    getTutorials() {
        let temp = this.http.get(this.tutorialsUrl).map((res:Response) => res.json());
        return temp;
    }

    getSingleTask(nid) {
        let temp = this.http.get(this.tutorialsUrl + nid).map((res:Response) => res.json());
        return temp;
    }

}