import { Component } from '@angular/core';
import {SearchService} from './search.service';
import {Observable} from 'rxjs/Rx';
import {FilterPipe} from '../../pipes/filter.pipe';
import {HomeService} from '../home/home.service';
import {DomSanitizer} from "@angular/platform-browser";
import { Router } from '@angular/router';

@Component({
	selector: 'search',
	pipes: [FilterPipe],
  	templateUrl: 'app/pages/search/search.html',
  	providers: [SearchService, HomeService],
})
export class SearchComponent { 	
	constructor(private router: Router; private domSanitizer : DomSanitizer, private _searchService: SearchService, private _homeService: HomeService) { }

	public tasks;
	public youtubeID;
	public youtubeUrl;
	public singleTasks;	

	goToHome(){
    	let link = ['/'];
        this.router.navigate(link);
    }

	ngOnInit() {
		this.getTutorials();
	}

	getTutorials() {
		this._searchService.getTutorials().subscribe(
		  
		  data => { this.tasks = data},
		 
		  err => console.error(err),
		  
		  () => console.log('All Tasks')
		);
	}

	getSingleTask(task) {
		console.log(task.title);
		this._homeService.getSingleTask(task.nid).subscribe(
		  
		  data => { this.singleTasks = data},
		 
		  err => console.error(err),
		  
		  () => console.log('done loading tasks')
		);		
	}




	getYoutube(task) {
		this.youtubeID = task.field_youtube_url;
		this.youtubeUrl = this.domSanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/' + this.youtubeID);
	}

}

