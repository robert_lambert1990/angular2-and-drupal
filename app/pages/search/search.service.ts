import { Injectable }     from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import {Observable} from 'rxjs/Rx';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class SearchService {

    constructor (private http: Http) {}

    private tutorialsUrl = '/drupal/api/all-tutorials/';
    private trueUrl1 = window.location.href.split("/");
    private trueUrl2;

    getUrl() {
    	this.trueUrl2 = this.trueUrl1[0] + '//' + this.trueUrl1[2] + this.tutorialsUrl;
    }

     getTutorials() {
     	this.getUrl();
        let temp = this.http.get(this.trueUrl2).map((res:Response) => res.json());        
        return temp;
      
    }

}