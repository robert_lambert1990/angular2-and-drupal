import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
	selector: 'error',
  	templateUrl: 'app/pages/error/error.html',
})
export class PageNotFoundComponent {
	constructor(private router: Router){}

	goToHome(){
    	let link = ['/'];
        this.router.navigate(link);
    }

    errorMessage = "You have reached the wrong page";
 }