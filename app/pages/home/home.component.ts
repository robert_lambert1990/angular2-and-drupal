import {Component} from '@angular/core';
import {HomeService} from './home.service';
import {Observable} from 'rxjs/Rx';
import {DomSanitizer} from "@angular/platform-browser";
import { Router } from '@angular/router';

@Component({
	selector: 'home',
    templateUrl: 'app/pages/home/home.html',
    providers: [HomeService]
})
export class HomeComponent {

	public singleTasks;
	public topLevelCategories;
	public selectedCategories;
	public selectedTopLevelCategory;
	public youtubeID;
	public youtubeUrl;
	public activeCheck;

  	constructor(private router: Router; private domSanitizer : DomSanitizer, private _homeService: HomeService) { }

    goToSearch(){
    	let link = ['/search'];
        this.router.navigate(link);
    }

	ngOnInit() {
		this.getTopLevelCategories();
	}

	getTopLevelCategories() {
		this._homeService.getTopLevelCategories().subscribe(
		  
		  data => { this.topLevelCategories = data},
		 
		  err => console.error(err),
		  
		  () => console.log('done loading tasks')
		);

	}

	getTutorialsFromSelected(selected) {
		this.topLevelCategories = [];
		this.singleTasks = [];
		this.selectedTopLevelCategory = selected;
		this._homeService.getTutorialsFromSelected(selected.tid).subscribe(
		  
		  data => { this.selectedCategories = data},
		 
		  err => console.error(err),
		  
		  () => console.log('done loading tasks')
		);		

		
	}

	getSingleTask(task) {
		console.log(task.title);
		this._homeService.getSingleTask(task.nid).subscribe(
		  
		  data => { this.singleTasks = data},
		 
		  err => console.error(err),
		  
		  () => console.log('done loading tasks')
		);	

	}

	getYoutube(task) {
		this.youtubeID = task.field_youtube_url;
		this.youtubeUrl = this.domSanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/' + this.youtubeID);
	}

	topLevelGoBack() {
		this.selectedCategories = [];
		this.singleTasks = [];
		this.getTopLevelCategories();
		this.activeCheck = '';
	}


}
