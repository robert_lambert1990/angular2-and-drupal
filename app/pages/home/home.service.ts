import { Injectable }     from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import {Observable} from 'rxjs/Rx';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class HomeService {

    constructor (private http: Http) {}

    private trueUrl1 = window.location.href.split("/");
    private trueUrl2;

    private topLevelCategoriesUrl = '/drupal/api/top-level-categories/';
    private tutorialsUrl = '/drupal/api/all-tutorials/';
    private postUrl = '/drupal/entity/node/';
    private tutorialSelectedUrl = '/drupal/api/content-related-to-taxonomy/'

    getUrl(urlName) {
        this.trueUrl2 = this.trueUrl1[0] + '//' + this.trueUrl1[2] + urlName;
    }

    getTopLevelCategories() {
        this.getUrl(this.topLevelCategoriesUrl);
        let temp = this.http.get(this.trueUrl2).map((res:Response) => res.json());
        return temp;
    }

    getTutorialsFromSelected(tid) {
        this.getUrl(this.tutorialSelectedUrl);
        let temp = this.http.get(this.trueUrl2 + tid).map((res:Response) => res.json());
        return temp;
    }

    getSingleTask(nid) {
        this.getUrl(this.tutorialsUrl);
        let temp = this.http.get(this.trueUrl2 + nid).map((res:Response) => res.json());
        return temp;
    }

}