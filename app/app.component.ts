import {Component} from '@angular/core';
import {AppService} from './app.service';
import {Observable} from 'rxjs/Rx';
import { Router } from '@angular/router';

@Component({
    selector: 'my-app',
    templateUrl: 'app/templates/tasklist.html',
})
export class AppComponent {
	constructor(private router: Router) {}

	goToHome(){
    	let link = ['/'];
        this.router.navigate(link);
    }

    goToSearch(){
    	let link = ['/search'];
        this.router.navigate(link);
    }

}
